package cz.berescak.aoc2019.day1;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Day1 {
    public static void main(String[] args) throws IOException {
        System.out.println("Part I - Test");
        countPart1("src/main/resources/inputs/day1test");
        System.out.println("Part I");
        countPart1("src/main/resources/inputs/day1");
        System.out.println("Part II - Test");
        countPart2("src/main/resources/inputs/day1testB");
        System.out.println("Part II") ;
        countPart2("src/main/resources/inputs/day1");


    }

    private static void countPart1(String path) throws IOException {
        File f = new File(path);
        List<String> lines = FileUtils.readLines(f, "UTF-8");

        int sum = lines.stream()
                .mapToInt(Integer::parseInt)
                .map(i -> (i / 3) - 2)
                .sum();

        System.out.println("SUM: " + sum);
    }

    private static void countPart2(String path) throws IOException {
        File f = new File(path);
        List<String> lines = FileUtils.readLines(f, "UTF-8");

        int sum = lines.stream()
                .mapToInt(Integer::parseInt)
                .map(i -> {
                    int localSum = 0;
                    while (true) {
                        i = (i / 3) - 2;
                        if (i <= 0) break;
                        localSum += i;
                    }
                    return localSum;

                })
                .sum();

        System.out.println("SUM: " + sum);
    }
}
