package cz.berescak.aoc2019.day10;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Precision;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day10 {
    public static void main(String[] args) throws IOException {
        countI("src/main/resources/inputs/day10.1");
        countI("src/main/resources/inputs/day10.2");
        countI("src/main/resources/inputs/day10.3");
        countI("src/main/resources/inputs/day10.4");
        countI("src/main/resources/inputs/day10.5");
        countI("src/main/resources/inputs/day10");

    }

    private static void countI(String path) throws IOException {
        boolean[][] matrix = loadMatrix(path);

        int besCount = Integer.MIN_VALUE;
        int bestX = -1;
        int bestY = -1;

        //printMatrix(matrix);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j]) {
                    int visibles = countVisible(i,j, matrix);


                    if(visibles > besCount) {
                        besCount =visibles;
                        bestX = i;
                        bestY = j;
                    }
                }
            }
        }
        System.out.println(besCount + " " + bestX + " " + bestY);
    }

    private static int countVisible(int bi, int bj, boolean[][] matrix) {

        Set<String> set = new HashSet<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(!matrix[i][j]) continue;
                if (i == bi && j == bj) continue;




                double dx = i - bi;
                double dy = j - bj;

                double len = Math.sqrt(dx * dx + dy * dy);

                String hash = "" + Precision.round(dx/len, 6) + ";" +  Precision.round(dy/len, 6);

                set.add(hash);

            }
        }
        return set.size();
    }

    private static boolean[][] loadMatrix(String path) throws IOException {
        File f = new File(path);
        List<String> lines = FileUtils.readLines(f, "UTF-8");

        boolean[][] matrix = new boolean[lines.size()][lines.get(0).length()];

        for (int i = 0; i < matrix.length; i++) {
            String line = lines.get(i);
            for (int j = 0; j < line.length(); j++) matrix[i][j] = line.charAt(j) == '#';
        }
        return matrix;
    }

    private static void printMatrix(boolean[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) System.out.print(matrix[i][j] ? "#" : ".");
            System.out.println();
        }
    }
}
