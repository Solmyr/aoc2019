package cz.berescak.aoc2019.day12;

import java.util.ArrayList;
import java.util.List;

public class Day12 {
    public static final int TYPE_POS_INDEX = 0;
    public static final int TYPE_VEL_INDEX = 1;

    public static final int AXE_X_INDEX = 0;
    public static final int AXE_Y_INDEX = 1;
    public static final int AXE_Z_INDEX = 2;


    public static void main(String[] args) {
        int[][][] testPlanets = {
                {
                        {-1, 0, 2},
                        {0, 0, 0}
                },
                {
                        {2, -10, -7},
                        {0, 0, 0}
                },
                {
                        {4, -8, 8},
                        {0, 0, 0}
                },
                {
                        {3, 5, -1},
                        {0, 0, 0}
                }

        };

        int[][][] planets = {
                {
                        {13, -13, -2},
                        {0, 0, 0}
                },
                {
                        {16, 2, -15},
                        {0, 0, 0}
                },
                {
                        {7, -18, -12},
                        {0, 0, 0}
                },
                {
                        {-  3, -8, -8},
                        {0, 0, 0}
                }

        };

        solveA(planets, 1000);
    }

    private static void solveA(int[][][] planets, int iterationCount) {

        printPlanets(planets, 0);

        for (int it = 0; it < iterationCount; it++) {
            int[][][] newPlanets = new int[planets.length][2][3];

            for (int i = 0; i < planets.length; i++) {
                for (int j = 0; j < planets.length; j++) {
                    if (i == j) continue;
                    newPlanets[i][TYPE_VEL_INDEX][AXE_X_INDEX] += Integer.compare(planets[j][TYPE_POS_INDEX][AXE_X_INDEX], planets[i][TYPE_POS_INDEX][AXE_X_INDEX]);
                    newPlanets[i][TYPE_VEL_INDEX][AXE_Y_INDEX] += Integer.compare(planets[j][TYPE_POS_INDEX][AXE_Y_INDEX], planets[i][TYPE_POS_INDEX][AXE_Y_INDEX]);
                    newPlanets[i][TYPE_VEL_INDEX][AXE_Z_INDEX] += Integer.compare(planets[j][TYPE_POS_INDEX][AXE_Z_INDEX], planets[i][TYPE_POS_INDEX][AXE_Z_INDEX]);
                }

                newPlanets[i][TYPE_VEL_INDEX][AXE_X_INDEX] += planets[i][TYPE_VEL_INDEX][AXE_X_INDEX];
                newPlanets[i][TYPE_VEL_INDEX][AXE_Y_INDEX] += planets[i][TYPE_VEL_INDEX][AXE_Y_INDEX];
                newPlanets[i][TYPE_VEL_INDEX][AXE_Z_INDEX] += planets[i][TYPE_VEL_INDEX][AXE_Z_INDEX];

                newPlanets[i][TYPE_POS_INDEX][AXE_X_INDEX] += newPlanets[i][TYPE_VEL_INDEX][AXE_X_INDEX] + planets[i][TYPE_POS_INDEX][AXE_X_INDEX];
                newPlanets[i][TYPE_POS_INDEX][AXE_Y_INDEX] += newPlanets[i][TYPE_VEL_INDEX][AXE_Y_INDEX] + planets[i][TYPE_POS_INDEX][AXE_Y_INDEX];
                newPlanets[i][TYPE_POS_INDEX][AXE_Z_INDEX] += newPlanets[i][TYPE_VEL_INDEX][AXE_Z_INDEX] + planets[i][TYPE_POS_INDEX][AXE_Z_INDEX];


            }

            planets = newPlanets;

            printPlanets(planets, it + 1);
        }

        int energy = 0;
        for (int[][] planet : planets) {

            energy += (Math.abs(planet[TYPE_POS_INDEX][AXE_X_INDEX]) +
                    Math.abs(planet[TYPE_POS_INDEX][AXE_Y_INDEX]) +
                    Math.abs(planet[TYPE_POS_INDEX][AXE_Z_INDEX])) *
                    (Math.abs(planet[TYPE_VEL_INDEX][AXE_X_INDEX]) +
                            Math.abs(planet[TYPE_VEL_INDEX][AXE_Y_INDEX]) +
                                    Math.abs(planet[TYPE_VEL_INDEX][AXE_Z_INDEX]));
        }

        System.out.println("E: " + energy);


    }

    private static void printPlanets(int[][][] planets, int it) {
        System.out.println("Step: " + it);
        for (int[][] planet : planets) {
            System.out.println("pos=" +
                    " <x= " + planet[TYPE_POS_INDEX][AXE_X_INDEX] +
                    ", y= " + planet[TYPE_POS_INDEX][AXE_Y_INDEX] +
                    ", z= " + planet[TYPE_POS_INDEX][AXE_Z_INDEX] + ">, vel=<" +
                    "  x= " + planet[TYPE_VEL_INDEX][AXE_X_INDEX] + "," +
                    "  y= " + planet[TYPE_VEL_INDEX][AXE_Y_INDEX] +
                    ", z= " + planet[TYPE_VEL_INDEX][AXE_Z_INDEX] + ">");
        }
    }
}
