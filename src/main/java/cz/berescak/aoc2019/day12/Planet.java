package cz.berescak.aoc2019.day12;

import lombok.Data;

@Data
public class Planet {
    public Planet(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    private int x;
    private int y;
    private int z;


    private int vx;
    private int vy;
    private int vz;

    @Override
    public String toString() {
        return "pos=<x= " + x + ", y=  " + y + ", z= " + z + ">, vel=<x= " + vx + ", y= " + vy + ", z= " + vz + ">";
    }
}
