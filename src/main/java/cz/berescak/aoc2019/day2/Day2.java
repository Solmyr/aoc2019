package cz.berescak.aoc2019.day2;

import cz.berescak.aoc2019.day9.ProcessorV3;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day2 {
    public static void main(String[] args) throws IOException {
        ProcessorV3 p = new ProcessorV3();

        System.out.println("Part I - Test");
        System.out.println("Part I");
        countPart1("1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,5,23,2,23,6,27,1,27,5,31,2,6,31,35,1,5,35,39,2,39,9,43,1,43,5,47,1,10,47,51,1,51,6,55,1,55,10,59,1,59,6,63,2,13,63,67,1,9,67,71,2,6,71,75,1,5,75,79,1,9,79,83,2,6,83,87,1,5,87,91,2,6,91,95,2,95,9,99,1,99,6,103,1,103,13,107,2,13,107,111,2,111,10,115,1,115,6,119,1,6,119,123,2,6,123,127,1,127,5,131,2,131,6,135,1,135,2,139,1,139,9,0,99,2,14,0,0");
        System.out.println("Part II ");
        countPart2(19690720, "1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,5,23,2,23,6,27,1,27,5,31,2,6,31,35,1,5,35,39,2,39,9,43,1,43,5,47,1,10,47,51,1,51,6,55,1,55,10,59,1,59,6,63,2,13,63,67,1,9,67,71,2,6,71,75,1,5,75,79,1,9,79,83,2,6,83,87,1,5,87,91,2,6,91,95,2,95,9,99,1,99,6,103,1,103,13,107,2,13,107,111,2,111,10,115,1,115,6,119,1,6,119,123,2,6,123,127,1,127,5,131,2,131,6,135,1,135,2,139,1,139,9,0,99,2,14,0,0");


    }

    private static void countPart2(int expectedResult, String input) {

        ProcessorV1 p = new ProcessorV1();

        iLoop: for (int i = 0; i < 100; i++) {
            for (int j = 0; j <100; j++) {
                List<Integer> stack = Arrays.stream(input.split(",")).map(Integer::parseInt).collect(Collectors.toList());
                stack.set(1, i);
                stack.set(2, j);

                int result = p.process(stack).get(0);

                if(result == expectedResult) {
                    System.out.println("i: " + i + ", j: " + j + " result: " + result);
                    break iLoop;
                }
            }
        }
    }

    private static void countPart1(String input) {
        ProcessorV1 p = new ProcessorV1();
        Integer result = p.process(input).get(0);
        System.out.println("Part I: " + result);
    }
}
