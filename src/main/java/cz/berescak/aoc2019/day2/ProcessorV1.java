package cz.berescak.aoc2019.day2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProcessorV1 {
    public List<Integer> process(String globalStack) {
        List<Integer> stack = Arrays.stream(globalStack.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        return process(stack);

    }

    public List<Integer> process(List<Integer> globalStack) {
        List<Integer> stack = new ArrayList<>(globalStack);
        int headPointer = 0;

        mainLoop:
        while (headPointer < stack.size()) {
            switch (stack.get(headPointer)) {
                case 1: {
                    headPointer += doAdd(headPointer, stack);
                    break;
                }
                case 2: {
                    headPointer += doMul(headPointer, stack);
                    break;
                }
                case 99: {
                    break mainLoop;
                }

                default: {
                    System.out.println("Neznama instrukce: " + stack.get(headPointer));
                    System.exit(-1);
                }

            }
        }
        return stack;
    }

    private static int doMul(int pointer, List<Integer> stack) {
        stack.set(stack.get(pointer + 3), stack.get(stack.get(pointer + 1)) * stack.get(stack.get(pointer + 2)));
        return 4;
    }

    private static int doAdd(int pointer, List<Integer> stack) {
        stack.set(stack.get(pointer + 3), stack.get(stack.get(pointer + 1)) + stack.get(stack.get(pointer + 2)));
        return 4;
    }
}
