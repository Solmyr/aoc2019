package cz.berescak.aoc2019.day3;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Point {
    private int x;
    private int y;
    private int stepCount;

    public int getManhattanDist() {
        return  Math.abs(getX()) + Math.abs(getY());
    }
}
