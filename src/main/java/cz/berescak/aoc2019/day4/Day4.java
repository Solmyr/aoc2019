package cz.berescak.aoc2019.day4;

import java.util.ArrayList;
import java.util.List;

public class Day4 {
    public static void main(String[] args) {
        solve(109165, 576723);
    }

    private static void solve(int from, int to) {
        int sum1 = 0;
        int sum2 = 0;

        for (int i = from; i <= to; i++) if (testRulesI(i)) sum1++;
        for (int i = from; i <= to; i++) if (testRulesII(i)) sum2++;

        System.out.println(sum1);
        System.out.println(sum2);
    }

    private static boolean testRulesI(int number) {
        String sNumber = "" + number;
        if (!testRule1A(sNumber)) return false;

        for (int i = 0; i < 5; i++)
            if (Integer.parseInt("" + sNumber.charAt(i)) > Integer.parseInt("" + sNumber.charAt(i + 1))) return false;
        return true;
    }

    private static boolean testRulesII(int number) {
        String sNumber = "" + number;

        if (!testRule2A(sNumber)) return false;

        for (int i = 0; i < 5; i++)
            if (Integer.parseInt("" + sNumber.charAt(i)) > Integer.parseInt("" + sNumber.charAt(i + 1))) return false;
        return true;
    }

    private static boolean testRule1A(String sNumber) {
        List<Integer> list = parseForRepeats(sNumber);
        for (Integer count : list) if (count > 1) return true;
        return false;
    }

    private static boolean testRule2A(String sNumber) {
        List<Integer> list = parseForRepeats(sNumber);
        for (Integer count : list) if (count == 2) return true;
        return false;
    }

    private static List<Integer> parseForRepeats(String sNumber) {
        List<Integer> repeats = new ArrayList<>();

        int counter = 1;
        char actual = sNumber.charAt(0);
        for (int i = 1; i < sNumber.length(); i++) {
            if (sNumber.charAt(i) == actual) {
                counter++;
            } else {
                repeats.add(counter);
                counter = 1;
                actual = sNumber.charAt(i);
            }
        }
        repeats.add(counter);
        return repeats;
    }
}
