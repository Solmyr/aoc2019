package cz.berescak.aoc2019.day5;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProcesorV2Parallel extends ProcessorV2 implements Runnable {
    private List<Integer> transferGlobalStack;
    private List<Integer> transferInputStack;
    private List<Integer> transferOutputStack;



    public List<Integer> processParallel(List<Integer> globalStack, List<Integer> inputStack, List<Integer> outputStack) {
        this.transferInputStack = inputStack;
        this.transferOutputStack = outputStack;
        this.transferGlobalStack = globalStack;

        Thread t = new Thread(this);
        t.run();
        return null;

    }

    public List<Integer> processParallel(String globalStack, List<Integer> inputStack, List<Integer> outputStack) {
        List<Integer> stack = Arrays.stream(globalStack.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        return process(stack, inputStack, outputStack);
    }

    @Override
    public void run() {
        process(transferGlobalStack,transferInputStack,transferOutputStack);
    }


}
