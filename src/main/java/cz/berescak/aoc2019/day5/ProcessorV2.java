package cz.berescak.aoc2019.day5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProcessorV2 {
    public List<Integer> process(String globalStack, List<Integer> inputStack, List<Integer> outputStack) {
        List<Integer> stack = Arrays.stream(globalStack.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        return process(stack, inputStack, outputStack);

    }

    public List<Integer> process(List<Integer> globalStack, List<Integer> inputStack, List<Integer> outputStack) {
        List<Integer> stack = new ArrayList<>(globalStack);
        int headPointer = 0;


        int counter = 0;
        mainLoop:
        while (headPointer < stack.size()) {
            System.out.println(counter);
            if(counter == 19) {
                System.out.println("xxx");
            }
            int instructionNumber = getInstructionNumber(stack.get(headPointer));
            switch (instructionNumber) {
                case 1: {
                    headPointer += doAdd(headPointer, stack);
                    break;
                }
                case 2: {
                    headPointer += doMul(headPointer, stack);
                    break;
                }
                case 3: {
                    headPointer += doInput(headPointer, stack, inputStack);
                    break;
                }
                case 4: {
                    headPointer += doOutput(headPointer, stack, outputStack);
                    break;
                }
                case 5: {
                    headPointer += ifTrue(headPointer, stack);
                    break;
                }
                case 6: {
                    headPointer += ifFalse(headPointer, stack);
                    break;
                }
                case 7: {
                    headPointer += lessThan(headPointer, stack);
                    break;
                }
                case 8: {
                    headPointer += equals(headPointer, stack);
                    break;
                }

                case 99: {
                    break mainLoop;
                }

                default: {
                    System.out.println("Neznama instrukce: " + stack.get(headPointer));
                    System.exit(-1);
                }


            }
            counter++;

        }
        return stack;
    }

    private int equals(int headPointer, List<Integer> stack) {
        int operand1 = stack.get(resolveOperandAddress(headPointer, stack, 0));
        int operand2 = stack.get(resolveOperandAddress(headPointer, stack, 1));
        if(operand1 == operand2)   stack.set(resolveOperandAddress(headPointer, stack, 2), 1);
        else stack.set(resolveOperandAddress(headPointer, stack, 2), 0);
        return 4;
    }

    private int lessThan(int headPointer, List<Integer> stack) {
        int operand1 = stack.get(resolveOperandAddress(headPointer, stack, 0));
        int operand2 = stack.get(resolveOperandAddress(headPointer, stack, 1));
        if(operand1 < operand2)   stack.set(resolveOperandAddress(headPointer, stack, 2), 1);
        else stack.set(resolveOperandAddress(headPointer, stack, 2), 0);
        return 4;
    }

    private int ifTrue(int headPointer, List<Integer> stack) {
        int operand1 = stack.get(resolveOperandAddress(headPointer, stack, 0));
        if(operand1 == 0) {
            return 3;
        } else {
            Integer operand2 = stack.get(resolveOperandAddress(headPointer, stack, 1));
            return operand2 - headPointer;
        }
    }

    private int ifFalse(int headPointer, List<Integer> stack) {
        int operand1 = stack.get(resolveOperandAddress(headPointer, stack, 0));
        if(operand1 == 0) {
            Integer operand2 = stack.get(resolveOperandAddress(headPointer, stack, 1));
            return operand2 - headPointer;
        } else {
            return 3;
        }
    }

    private int getInstructionNumber(Integer instruction) {
        return instruction % 100;
    }

    private int doOutput(int headPointer, List<Integer> stack, List<Integer> outputStack) {
        outputStack.add(stack.get(resolveOperandAddress(headPointer, stack, 0)));
        return 2;
    }

    private int doInput(int headPointer, List<Integer> stack, List<Integer> inputStack) {
        stack.set(resolveOperandAddress(headPointer, stack, 0), inputStack.get(0));
        inputStack.remove(0);
        return 2;
    }

    private int doMul(int pointer, List<Integer> stack) {
        int operand1 = stack.get(resolveOperandAddress(pointer, stack, 0));
        int operand2 = stack.get(resolveOperandAddress(pointer, stack, 1));

        stack.set(resolveOperandAddress(pointer, stack, 2), operand1 * operand2);
        return 4;
    }

    private Integer resolveOperandAddress(int pointer, List<Integer> stack, int paramIndex) {
        int instruction = stack.get(pointer);
        int parameter = instruction / 100 / (int) Math.pow(10, paramIndex);
        parameter %= 10;

        switch (parameter) {
            case 0: {
                return stack.get(pointer + paramIndex + 1);
            }
            case 1: {
                return pointer + paramIndex + 1;
            }
        }
        System.out.println("neznamy typ parametru");
        System.exit(-2);
        return 0;

    }

    private int doAdd(int pointer, List<Integer> stack) {
        int operand1 = stack.get(resolveOperandAddress(pointer, stack, 0));
        int operand2 = stack.get(resolveOperandAddress(pointer, stack, 1));

        stack.set(stack.get(pointer + 3), operand1 + operand2);
        return 4;
    }
}
