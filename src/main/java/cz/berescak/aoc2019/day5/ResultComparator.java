package cz.berescak.aoc2019.day5;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ResultComparator {
    public static void main(String[] args) throws IOException {
        List<String> list1 = loadList("src/main/resources/debug/xxx1");
        List<String> list2 = loadList("src/main/resources/debug/xxx2");


        mainLoop: for (int i = 0; i < list1.size(); i++) {
            System.out.println("Pruzkum v cyklu:" + i);

            if(!list1.get(i).equals(list2.get(i))) {


                System.out.println(list1.get(i));
                System.out.println(list2.get(i));

                List<Integer> parts1 = Arrays.asList(list1.get(i).split(",")).stream().map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
                List<Integer> parts2 = Arrays.asList(list2.get(i).split(",")).stream().map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());

                if(parts1.size() != parts2.size()) {
                    System.out.println("Nesouhalsi pocet clenu");
                    break;
                }

                for (int j = 0; j < parts1.size(); j++) {
                    if(!parts1.get(j).equals(parts2.get(j))) {
                        System.out.println("Nesouhlasi prvek na indexu: " + j + " 1: " + parts1.get(j) + " 2: " + parts2.get(j));
                        System.out.println(parts1);
                        System.out.println(parts2);
                        break mainLoop;
                    }
                }
                
                break;
            }
        }

    }

    private static List<String> loadList(String path) throws IOException {
        File f = new File(path);
        return  FileUtils.readLines(f, "UTF-8");
    }
}
