package cz.berescak.aoc2019.day6;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Day6 {
    private static Orbit root;
    private static HashMap<String, Orbit> orbitMap;

    public static void main(String[] args) throws IOException {
        String path = "src/main/resources/inputs/day6TestB";
        loadNodes(path);
        setDistances(root, 0, new ArrayList<>());
        System.out.println(orbitMap.values().stream().mapToInt(o -> o.getRootDistance()).sum());

        ArrayList<String> youPathCopy = new ArrayList<>(orbitMap.get("YOU").getPathToRoot());
        ArrayList<String> sanPathCopy = new ArrayList<>(orbitMap.get("SAN").getPathToRoot());

        youPathCopy.removeAll(orbitMap.get("SAN").getPathToRoot());
        sanPathCopy.removeAll(orbitMap.get("YOU").getPathToRoot());

        int distance = youPathCopy.size() + sanPathCopy.size();
        System.out.println(distance);

    }

    private static void setDistances(Orbit root, int distance, ArrayList<String> pathToRoot) {
        root.setPathToRoot(new ArrayList<>(pathToRoot));
        pathToRoot.add(root.getName());
        root.setRootDistance(distance);
        distance++;
        int finalDistance = distance;
        root.getOrbits().stream().forEach(o -> setDistances(o, finalDistance, new ArrayList<>(pathToRoot)));
    }


    private static void loadNodes(String path) throws IOException {
        File f = new File(path);
        List<String> lines = FileUtils.readLines(f, "UTF-8");
        orbitMap = new HashMap<>();

        Orbit o = new Orbit();
        o.setName("COM");
        o.setOrbits(new HashSet<>());
        orbitMap.put(o.getName(), o);

        for (String line : lines) {
            String[] splited = line.split("\\)");

            o = new Orbit();
            o.setName(splited[1]);
            o.setOrbits(new HashSet<>());
            orbitMap.put(o.getName(), o);


        }

        for (String line : lines) {
            String[] splited = line.split("\\)");
            Orbit parent = orbitMap.get(splited[0]);
            Orbit child = orbitMap.get(splited[1]);

            parent.getOrbits().add(child);
            child.setParentOrbit(parent);
        }

        root = orbitMap.get("COM");

    }
}
