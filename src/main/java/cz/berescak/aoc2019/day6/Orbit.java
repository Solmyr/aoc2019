package cz.berescak.aoc2019.day6;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
import java.util.Set;

@Data
public class Orbit {
    private String name;
    @ToString.Exclude @EqualsAndHashCode.Exclude private  Orbit parentOrbit;
    private Set<Orbit> orbits;
    private int rootDistance;
    private List<String> pathToRoot;
}
