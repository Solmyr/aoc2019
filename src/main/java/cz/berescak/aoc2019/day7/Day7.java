package cz.berescak.aoc2019.day7;

import cz.berescak.aoc2019.day5.ProcessorV2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day7 {
    private static String instructionsString = "3,8,1001,8,10,8,105,1,0,0,21,34,55,68,85,106,187,268,349,430,99999,3,9,1001,9,5,9,1002,9,5,9,4,9,99,3,9,1002,9,2,9,1001,9,2,9,1002,9,5,9,1001,9,2,9,4,9,99,3,9,101,3,9,9,102,3,9,9,4,9,99,3,9,1002,9,5,9,101,3,9,9,102,5,9,9,4,9,99,3,9,1002,9,4,9,1001,9,2,9,102,3,9,9,101,3,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,99";

    public static void main(String[] args) {
        resolvePartI();
        resolvePartII();

    }

    private static void resolvePartII() {
        ProcessorV2 p = new ProcessorV2();

        List<List<Integer>> permutations = Permutations.of(Arrays.asList(5, 6, 7, 8, 9)).map(s -> s.collect(Collectors.toList())).collect(Collectors.toList());

        int bestValue = Integer.MIN_VALUE;
        List<Integer> bestPermutation = null;

        for (List<Integer> permutation : permutations) {
            List<Integer> outputs = new ArrayList<>();
            outputs.add(0);
            List<Integer> inputs = new ArrayList<>();

            for (int i = 0; i < 5; i++) {
                inputs = new ArrayList<>();
                inputs.add(permutation.get(i));
                inputs.addAll(outputs);
                outputs = new ArrayList<>();

                p.process(instructionsString, inputs, outputs);
            }



            int value = outputs.get(0);

            if(value > bestValue) {
                bestPermutation = permutation;
                bestValue = value;
            }

        }

        System.out.println(bestPermutation + " " + bestValue);
    }

    private static void resolvePartI() {
        ProcessorV2 p = new ProcessorV2();

        List<List<Integer>> permutations = Permutations.of(Arrays.asList(0, 1, 2, 3, 4)).map(s -> s.collect(Collectors.toList())).collect(Collectors.toList());

        int bestValue = Integer.MIN_VALUE;
        List<Integer> bestPermutation = null;

        for (List<Integer> permutation : permutations) {
            List<Integer> outputs = new ArrayList<>();
            outputs.add(0);
            List<Integer> inputs = new ArrayList<>();

            for (int i = 0; i < 5; i++) {
                inputs = new ArrayList<>();
                inputs.add(permutation.get(i));
                inputs.add(outputs.get(0));
                outputs = new ArrayList<>();

                p.process(instructionsString, inputs, outputs);
            }
            int value = outputs.get(0);

            if(value > bestValue) {
                bestPermutation = permutation;
                bestValue = value;
            }

        }

        System.out.println(bestPermutation + " " + bestValue);
    }
}
