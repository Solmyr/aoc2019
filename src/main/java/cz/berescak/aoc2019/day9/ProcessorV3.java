package cz.berescak.aoc2019.day9;


import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

public class ProcessorV3 implements Runnable {
    private TwoLayerStack stack;
    private Queue<Long> inputStack;
    private Queue<Long> outputStack;
    private boolean async;

    @Getter
    private boolean running;

    public List<Long> process(String globalStack, Queue<Long> inputStack, Queue<Long> outputStack) {
        stack = new TwoLayerStack(Arrays.stream(globalStack.split(",")).map(Long::parseLong).collect(Collectors.toList()));
        this.inputStack = inputStack;
        this.outputStack = outputStack;
        async = false;
        process();
        return stack.toList();
    }

    public void processInThread(String globalStack, BlockingQueue<Long> inputStack, BlockingQueue<Long> outputStack) {
        stack = new TwoLayerStack(Arrays.stream(globalStack.split(",")).map(Long::parseLong).collect(Collectors.toList()));
        this.inputStack = inputStack;
        this.outputStack = outputStack;
        async = true;
        running = true;
        Thread t = new Thread(this, "Procesor thread");
        t.start();

    }

    @Override
    public void run() {
        process();
    }


    private void process() {

        mainLoop:
        while (true) {
            long instructionNumber = getInstructionNumber(stack.getActualInstruction());
            switch ((int) instructionNumber) {
                case 1: {
                    stack.addToInstructionPointer(doAdd());
                    break;
                }
                case 2: {

                    stack.addToInstructionPointer(doMul());
                    break;
                }
                case 3: {
                    stack.addToInstructionPointer(doInput());
                    break;
                }
                case 4: {
                    stack.addToInstructionPointer(doOutput());
                    break;
                }
                case 5: {
                    stack.addToInstructionPointer(ifTrue());
                    break;
                }
                case 6: {
                    stack.addToInstructionPointer(ifFalse());
                    break;
                }
                case 7: {
                    stack.addToInstructionPointer(lessThan());
                    break;
                }
                case 8: {
                    stack.addToInstructionPointer(equals());
                    break;
                }
                case 9: {
                    stack.addToInstructionPointer(changeRelativeBase());
                    break;
                }

                case 99: {
                    running = false;
                    outputStack.add(0L);
                    break mainLoop;
                }

                default: {
                    System.out.println("Neznama instrukce: " + stack.getActualInstruction());
                    System.exit(-1);
                }


            }

        }

    }

    private int changeRelativeBase() {
        long operand1 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 0));
        stack.addToRelativeBase(operand1);
        return 2;
    }

    private long equals() {
        long operand1 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 0));
        long operand2 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 1));
        if (operand1 == operand2) stack.set(resolveOperandAddress(stack.getInstructionPointer(), stack, 2), 1L);
        else stack.set(resolveOperandAddress(stack.getInstructionPointer(), stack, 2), 0L);
        return 4;
    }

    private int lessThan() {
        long operand1 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 0));
        long operand2 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 1));
        if (operand1 < operand2) stack.set(resolveOperandAddress(stack.getInstructionPointer(), stack, 2), 1L);
        else stack.set(resolveOperandAddress(stack.getInstructionPointer(), stack, 2), 0L);
        return 4;
    }

    private long ifTrue() {
        long operand1 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 0));
        if (operand1 == 0) {
            return 3;
        } else {
            Long operand2 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 1));
            return operand2 - stack.getInstructionPointer();
        }
    }

    private long ifFalse() {
        long operand1 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 0));
        if (operand1 == 0) {
            Long operand2 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 1));
            return operand2 - stack.getInstructionPointer();
        } else {
            return 3;
        }
    }

    private long getInstructionNumber(Long instruction) {
        return instruction % 100;
    }

    private int doOutput() {
        //System.out.println("PROCESOR: zapisuju na out");
        outputStack.add(stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 0)));
        return 2;
    }

    private int doInput() {
        try {
            //System.out.println("PROCESOR: cekam na in");
            long value = async ? ((BlockingQueue<Long>) inputStack).take() : inputStack.poll();
            stack.set(resolveOperandAddress(stack.getInstructionPointer(), stack, 0), value);
            //System.out.println("PROCESOR: dostal jsem in");


        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return 2;
    }

    private int doMul() {
        long operand1 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 0));
        long operand2 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 1));

        stack.set(resolveOperandAddress(stack.getInstructionPointer(), stack, 2), operand1 * operand2);
        return 4;
    }

    private long resolveOperandAddress(int pointer, TwoLayerStack stack, int paramIndex) {
        long instruction = stack.get(pointer);
        long parameter = instruction / 100 / (int) Math.pow(10, paramIndex);
        parameter %= 10;

        switch ((int) parameter) {
            case 0: {
                return stack.get(pointer + paramIndex + 1);
            }
            case 1: {
                return pointer + paramIndex + 1;
            }
            case 2: {
                return stack.get(pointer + paramIndex + 1) + stack.getRelativeBase();
            }
        }
        System.out.println("neznamy typ parametru");
        System.exit(-2);
        return 0;

    }

    private int doAdd() {
        long operand1 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 0));
        long operand2 = stack.get(resolveOperandAddress(stack.getInstructionPointer(), stack, 1));

        stack.set(resolveOperandAddress(stack.getInstructionPointer(), stack, 2), operand1 + operand2);
        return 4;
    }


}
