package cz.berescak.aoc2019.day9;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TwoLayerStack {

    @Getter
    private int relativeBase = 0;

    @Getter
    private int instructionPointer = 0;

    private final List<Long> stackList;
    private final Map<Long, Long> stackMap;
    private final int listSize;

    public TwoLayerStack(List<Long> globalStack) {
        stackList = new ArrayList<>(globalStack);
        stackMap = new HashMap<>();
        listSize = stackList.size();
    }

    public int size() {
        return stackList.size();
    }

    public long get(long pointer) {
        if (pointer < listSize) return stackList.get((int) pointer);
        if (stackMap.containsKey(pointer)) return stackMap.get(pointer);
        return 0L;
    }

    public void set(long pointer, Long value) {
        if (pointer >= listSize) {
            stackMap.put(pointer, value);
        } else {
            stackList.set((int) pointer, value);
        }
    }

    public List<Long> toList() {
        return stackList;
    }

    public void addToRelativeBase(long value) {
        relativeBase += value;
    }

    public void addToInstructionPointer(long value) {
        instructionPointer += value;
    }

    public Long getActualInstruction() {
        return this.get(instructionPointer);
    }

    @Override
    public String toString() {
        return "IP: " + instructionPointer + ", RB: " + relativeBase + ", AI: " + getActualInstruction() + ", stackList: " + stackList + ", stackMap: " + stackMap;
    }
}
